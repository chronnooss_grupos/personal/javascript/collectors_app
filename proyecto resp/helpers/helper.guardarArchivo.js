import fs from "fs";
import * as dotenv from "dotenv";
dotenv.config();

const pathArchivo = `${process.env.DB_JSON_PATH}`;

const guardarJSONHelper = (data) => {
  fs.writeFileSync(pathArchivo, JSON.stringify(data));
};

const leerJSONHelper = () => {
  if (!fs.existsSync(pathArchivo)) {
    return null;
  }

  const info = fs.readFileSync(pathArchivo, { encoding: "utf-8" });
  const data = JSON.parse(info);

  return data;
};

export { guardarJSONHelper, leerJSONHelper };
