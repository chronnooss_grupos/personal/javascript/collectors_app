import { guardarJSONHelper } from "./helper.guardarArchivo.js";
import { formatearFechaHelper } from "./helper.util.js";

const construirObjBusquedaVideojuegosHelper = async (
  ve_search,
  ve_platform,
  ve_categories
) => {
  let p_search = ve_search ?? "";
  let p_where = ve_platform ? ve_platform : process.env.IGDB_WHERE_PLATFORMS;

  if (ve_categories) {
    p_where += ` & ${ve_categories}`;
  }

  const objBusqueda = {
    fields: process.env.IGDB_FIELDS,
    search: p_search,
    where: p_where,
    limit: process.env.IGDB_LIMIT,
    sort: process.env.IGDB_SORT,
    exclude: process.env.IGDB_EXCLUDE,
    offset: process.env.IGDB_OFFSET,
  };

  return objBusqueda;
};

const construirQueryIGDBHelper = (objBusqueda) => {
  let vl_query = "";
  let { fields, search, where, limit, sort, exclude, offset } = objBusqueda;

  if (fields != "") {
    vl_query += `fields ${fields};`;
  }

  if (search != "") {
    vl_query += `search "${search}";`;
  }

  if (where != "") {
    vl_query += `where ${where};`;
  }

  if (limit != "") {
    vl_query += `limit ${limit};`;
  }

  if (sort != "") {
    vl_query += `sort ${sort};`;
  }

  if (exclude != "") {
    vl_query += `exclude ${exclude};`;
  }

  if (offset != "") {
    vl_query += `offset ${offset};`;
  }

  return vl_query;
};

const construirQueryOffsetIGDBHelper = (objBusqueda, ve_offset) => {
  let vl_query = "";
  let { fields, search, where, limit, sort, exclude, offset } = objBusqueda;

  if (fields != "") {
    vl_query += `fields ${fields};`;
  }

  if (search != "") {
    vl_query += `search "${search}";`;
  }

  if (where != "") {
    vl_query += `where ${where};`;
  }

  if (limit != "") {
    vl_query += `limit ${limit};`;
  }

  if (sort != "") {
    vl_query += `sort ${sort};`;
  }

  if (exclude != "") {
    vl_query += `exclude ${exclude};`;
  }

  if (ve_offset >= 0) {
    vl_query += `offset ${ve_offset};`;
  }

  return vl_query;
};

const construirArrPlataformasIGDBHelper = (coleccionPlataformas = []) => {
  const plataformaMapping = {
    NES: { name: "Nintendo", color: "red" },
    SNES: { name: "Super Nintendo", color: "red" },
    N64: { name: "Nintendo 64", color: "red" },
    NGC: { name: "Nintendo Gamecube", color: "red" },
    Wii: { name: "Nintendo Wii", color: "red" },
    WiiU: { name: "Nintendo Wii U", color: "red" },
    Switch: { name: "Nintendo Switch", color: "red" },
    GB: { name: "Game Boy", color: "red" },
    GBC: { name: "Game Boy Color", color: "red" },
    GBA: { name: "Game Boy Advance", color: "red" },
    NDS: { name: "Nintendo DS", color: "red" },
    "3DS": { name: "Nintendo 3DS", color: "red" },
    PS1: { name: "PlayStation 1", color: "blue" },
    PS2: { name: "PlayStation 2", color: "blue" },
    PS3: { name: "PlayStation 3", color: "blue" },
    PS4: { name: "PlayStation 4", color: "blue" },
    PS5: { name: "PlayStation 5", color: "blue" },
    PSP: { name: "PlayStation Portable", color: "blue" },
    Vita: { name: "PlayStation Vita", color: "blue" },
    PSVR: { name: "PlayStation VR", color: "blue" },
    PSVR2: { name: "PlayStation VR2", color: "blue" },
    XBOX: { name: "Xbox", color: "green" },
    X360: { name: "Xbox 360", color: "green" },
    XONE: { name: "Xbox One", color: "green" },
    "Series X": { name: "Xbox Series", color: "green" },
    PC: { name: "PC", color: "brown" },
    iOS: { name: "Mobile", color: "yellow" },
    Android: { name: "Mobile", color: "yellow" },
  };

  const arrPlatforms = [];

  for (const plataforma of coleccionPlataformas) {
    if (plataforma.abbreviation in plataformaMapping) {
      const { name, color } = plataformaMapping[plataforma.abbreviation];
      arrPlatforms.push({ abbrev: plataforma.abbreviation, name, color });
    } else {
      arrPlatforms.push({
        abbrev: plataforma.abbreviation,
        name: "Otra plataforma",
        color: "default",
      });
    }
  }

  return arrPlatforms;
};

const construirArrGenerosIGDBHelper = (coleccionGeneros) => {
  let arrGeneros = [];

  if (coleccionGeneros) {
    arrGeneros = coleccionGeneros.map((g) => ({
      name: g.name,
      color: "default",
    }));
  } else {
    arrGeneros = [
      {
        name: `S / I`,
        color: "default",
      },
    ];
  }

  return arrGeneros;
};

const construirArrDesarrolladorIGDBHelper = (coleccionDesarrollador) => {
  let arrDesarrollador = [];

  if (coleccionDesarrollador) {
    arrDesarrollador = coleccionDesarrollador
      .filter((d) => d.developer)
      .map((d) => ({
        name: d.company.name,
        color: "default",
      }));
  } else {
    arrDesarrollador = [
      {
        name: `S / I`,
        color: "default",
      },
    ];
  }

  return arrDesarrollador;
};

const construirArrPublicadorIGDBHelper = (coleccionPublicador) => {
  let arrPublicador = [];

  if (coleccionPublicador) {
    arrPublicador = coleccionPublicador
      .filter((d) => d.publisher)
      .map((d) => ({
        name: d.company.name,
        color: "default",
      }));
  } else {
    arrPublicador = [
      {
        name: `S / I`,
        color: "default",
      },
    ];
  }

  return arrPublicador;
};

const obtenerArrJuegosIGDBHelper = async (respJuegos, objVideojuegos) => {
  try {
    for (const juego of respJuegos) {
      let arrPlataformas = construirArrPlataformasIGDBHelper(juego.platforms);
      let arrGeneros = construirArrGenerosIGDBHelper(juego.genres);
      let arrDesarrollador = construirArrDesarrolladorIGDBHelper(
        juego.involved_companies
      );
      let arrPublicador = construirArrPublicadorIGDBHelper(
        juego.involved_companies
      );

      const idJuego = juego.id;
      const nomJuego = juego.name.replace(/"/g, "");
      const idImageJuego = juego.cover ? juego.cover.image_id : "";
      const urlImagenJuego = juego.cover
        ? `https://images.igdb.com/igdb/image/upload/t_720p/${idImageJuego}.jpg`
        : `${process.env.IGDB_IMAGE_EMPTY}`;
      const fechaSalida = juego.first_release_date
        ? formatearFechaHelper(juego.first_release_date)
        : "S/I";

      const objRespuesta = {
        id: idJuego,
        nombre: nomJuego,
        imagen: urlImagenJuego,
        plataformas: arrPlataformas,
        generos: arrGeneros,
        desarrollador: arrDesarrollador,
        publicador: arrPublicador,
        fecha: fechaSalida,
      };

      objVideojuegos.push(objRespuesta);
    }

    objVideojuegos.sort((a, b) =>
      a.nombre.toLowerCase().localeCompare(b.nombre.toLowerCase())
    );

    guardarJSONHelper(objVideojuegos);

    return objVideojuegos;
  } catch (error) {
    console.log("Error en obtenerArrJuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

const renderizarPanelGaleriaVideojuegosIGDBHelper = async (arrVideojuegos) => {
  try {
    const buildBadges = (items, className) =>
      items
        .map(
          (item) =>
            `<span class='badge rounded-pill badge-${
              item.color
            }'>${item.abbrev.toUpperCase()}</span>`
        )
        .join(" ");

    const buildOptions = (items) =>
      items
        .map(
          (item, index) =>
            `<option value="${index + 1}">${item.abbrev.toUpperCase()}</option>`
        )
        .join(" ");

    const buildGaleriaItems = (videojuegos) =>
      videojuegos
        .map(
          (element) => `
      <div class="galeria-item">
        <img class="galeria-picture" src="${element.imagen}" alt="${
            element.nombre
          }">
        <div class="content">
          <div class="short-desc">
            ${buildBadges(element.plataformas)}
          </div>
          <h5>${element.fecha}</h5>
          <h4>${element.nombre}</h4>
          <div class="footer">
            <a href="#"><i class="fa-solid fa-heart"></i></a>
            <a href="#"><i class="fa-solid fa-bookmark"></i></a>
            <a href="#"><i class="fa-solid fa-share"></i></a>
          </div>
        </div>
      </div>`
        )
        .join(" ");

    let htmlVideojuegos = `<div class="cardLibrary card container m-auto p-3"><div class="galeria" id="galeria">`;

    htmlVideojuegos += buildGaleriaItems(arrVideojuegos);

    htmlVideojuegos += `</div></div>`;

    return htmlVideojuegos;
  } catch (error) {
    console.log("Error en renderizarPanelGaleriaVideojuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

const renderizarPanelTablaVideojuegosIGDBHelper = async (arrVideojuegos) => {
  try {
    const buildBadges = (items, className) =>
      items
        .map(
          (item) =>
            `<span class='badge rounded-pill badge-${
              item.color
            }'>${item.abbrev.toUpperCase()}</span>`
        )
        .join(" ");

    const buildOptions = (items) =>
      items
        .map(
          (item, index) =>
            `<option value="${index + 1}">${item.abbrev.toUpperCase()}</option>`
        )
        .join(" ");

    const buildTablaItems = (videojuegos) =>
      videojuegos
        .map(
          (element) => `
          <tr>
          <td><img src="${element.imagen}" style="width: 70px;
          height: auto;
          /* width: 220px;
            height: 300px; */
          transition: 0.2s linear;
          transform-origin: 50% 50%;
          border-radius: 15px;" alt="Imagen"></td>
          <td>${element.nombre}</td>
          <td>${buildBadges(element.plataformas)}</td>
          <td>${element.fecha}</td>
        </tr>`
        )
        .join(" ");

    let htmlVideojuegos = `<table class='table'>
                            <thead>
                            <tr>
                              <th>Imagen</th>
                              <th>Título</th>
                              <th>Plataformas</th>
                              <th>Fecha</th>
                            </tr>
                          </thead>
                          <tbody>`;

    htmlVideojuegos += buildTablaItems(arrVideojuegos);

    htmlVideojuegos += `</tbody></table>`;

    return htmlVideojuegos;
  } catch (error) {
    console.log("Error en renderizarPanelTablaVideojuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

export {
  construirObjBusquedaVideojuegosHelper,
  construirQueryIGDBHelper,
  construirQueryOffsetIGDBHelper,
  construirArrPlataformasIGDBHelper,
  construirArrGenerosIGDBHelper,
  construirArrDesarrolladorIGDBHelper,
  construirArrPublicadorIGDBHelper,
  obtenerArrJuegosIGDBHelper,
  renderizarPanelGaleriaVideojuegosIGDBHelper,
  renderizarPanelTablaVideojuegosIGDBHelper,
};
