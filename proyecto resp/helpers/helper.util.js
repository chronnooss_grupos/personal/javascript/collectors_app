const formatearFechaHelper = (timestamp) => {
  // Crear un objeto de fecha a partir del timestamp en milisegundos
  const fecha = new Date(timestamp * 1000);

  // Obtener los componentes de la fecha
  const dia = fecha.getDate().toString().padStart(2, "0");
  const mes = (fecha.getMonth() + 1).toString().padStart(2, "0"); // Se suma 1 porque los meses comienzan desde 0
  const año = fecha.getFullYear();

  // Formatear la fecha como "dd/mm/yyyy"
  const fechaFormateada = `${dia}/${mes}/${año}`;

  return fechaFormateada;
};

export { formatearFechaHelper };
