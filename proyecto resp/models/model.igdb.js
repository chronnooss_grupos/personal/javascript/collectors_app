import * as dotenv from "dotenv";
import axios from "axios";
dotenv.config();
import {
  construirQueryIGDBHelper,
  construirQueryOffsetIGDBHelper,
  obtenerArrJuegosIGDBHelper,
} from "../helpers/helper.igdb.js";

const autenticarConIGDBModel = async () => {
  const response = await axios.post(`${process.env.IGDB_AUTH_URL}`);
  return response.data;
};

const cantidadJuegosIGDBModel = async (objBusqueda) => {
  // Obtener Access Token de IGDB
  let { access_token } = await autenticarConIGDBModel();

  const url = process.env.IGDB_ENDPOINT_CANTIDAD;
  const headers = {
    headers: {
      Accept: "application/json",
      "Client-ID": process.env.IGDB_CLIENT_ID,
      Authorization: `Bearer ${access_token}`,
    },
  };

  // Construir Query para IGDB
  const query = construirQueryIGDBHelper(objBusqueda);

  // Llamada a API IGDB para obtener cantidad de juegos
  const response = await axios.post(url, query, headers);

  console.log("response.data", response.data);

  return response;
};

const obtenerJuegosIGDBModel = async (objBusqueda, cantidadVideojuegos) => {
  try {
    const arrVideojuegos = [];
    let offset = 0;
    let totalCount = 0;

    // Obtener Access Token de IGDB
    const { access_token } = await autenticarConIGDBModel();
    const headers = {
      Accept: "application/json",
      "Client-ID": process.env.IGDB_CLIENT_ID,
      Authorization: `Bearer ${access_token}`,
    };

    do {
      // Setear parámetros para llamada de servicio
      const url = process.env.IGDB_ENDPOINT_JUEGOS;

      // Construir Query para IGDB
      const query = construirQueryOffsetIGDBHelper(objBusqueda, offset);

      const responseObtenerJuegos = await axios.post(url, query, { headers });

      const juegosObtenidos = await obtenerArrJuegosIGDBHelper(
        responseObtenerJuegos.data,
        arrVideojuegos
      );

      arrVideojuegos.push(...juegosObtenidos);
      totalCount += juegosObtenidos.length;
      offset += 500;
    } while (
      totalCount < cantidadVideojuegos &&
      arrVideojuegos.length < cantidadVideojuegos
    );

    return arrVideojuegos.slice(0, cantidadVideojuegos);
  } catch (error) {
    console.log("Error en obtenerJuegosIGDBModel", error);
    throw new Error(`${error.body}`);
  }
};

export { cantidadJuegosIGDBModel, obtenerJuegosIGDBModel };
