import * as dotenv from "dotenv";
dotenv.config();
import {
  construirObjBusquedaVideojuegosHelper,
  renderizarPanelGaleriaVideojuegosIGDBHelper,
  renderizarPanelTablaVideojuegosIGDBHelper,
} from "../helpers/helper.igdb.js";

import {
  cantidadJuegosIGDBModel,
  obtenerJuegosIGDBModel,
} from "./model.igdb.js";

const busquedaVideojuegosModel = async (search, platforms, categories) => {
  try {
    const parametrosBusqueda = await construirObjBusquedaVideojuegosHelper(
      search,
      platforms,
      categories
    );

    const respuestaCantidadVideojuegos = await cantidadJuegosIGDBModel(
      parametrosBusqueda
    );

    const cantidadVideojuegos = respuestaCantidadVideojuegos.data.count;
    const htmlCantidadVideojuegos = `<h3>Total de juegos: ${cantidadVideojuegos}</h3>`;

    const respuestaObtenerVideojuegos = await obtenerJuegosIGDBModel(
      parametrosBusqueda,
      cantidadVideojuegos
    );
    // Renderizar Panel Galería de Videojuegos
    const htmlPanelGaleriaVideojuegos =
      await renderizarPanelGaleriaVideojuegosIGDBHelper(
        respuestaObtenerVideojuegos
      );

    // Renderizar Panel Tabla de Videojuegos
    const htmlPanelTablaVideojuegos =
      await renderizarPanelTablaVideojuegosIGDBHelper(
        respuestaObtenerVideojuegos
      );

    return {
      panelGaleriaVideojuegos: htmlPanelGaleriaVideojuegos,
      panelTablaVideojuegos: htmlPanelTablaVideojuegos,
      cantidadVideojuegos: htmlCantidadVideojuegos,
    };
  } catch (error) {
    console.log("Error en busquedaVideojuegosModel", error);
    throw new Error("Error en la búsqueda de videojuegos.");
  }
};

export { busquedaVideojuegosModel };
