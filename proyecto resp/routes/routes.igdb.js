import { Router } from "express";
import { renderizarVideojuegosController } from "../controllers/controller.igdb.js";

const routerIGDB = Router();

routerIGDB.post("/renderizarVideojuegos", renderizarVideojuegosController);

export { routerIGDB };
