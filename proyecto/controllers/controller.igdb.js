import { response } from "express";
import * as dotenv from "dotenv";
dotenv.config();
import { busquedaVideojuegosIGDBModel } from "../models/model.videojuego.js";

const renderizarVideojuegosController = async (req, res = response) => {
  try {
    // Se obtienen parámetros de búsqueda
    let { search, platforms, categories } = req.body;

    // Se obtienen los registros de videojuegos desde IGDB
    const {
      panelGaleriaVideojuegos,
      panelTablaVideojuegos,
      cantidadVideojuegos,
    } = await busquedaVideojuegosIGDBModel(search, platforms, categories);

    // Se envía la respuesta con los registros obtenidos
    res.status(200).json({
      msg: "Registros obtenidos correctamente.",
      htmlTotalJuegos: cantidadVideojuegos,
      htmlPanelGaleriaVideojuegos: panelGaleriaVideojuegos,
      htmlPanelTablaVideojuegos: panelTablaVideojuegos,
    });
  } catch (error) {
    console.log("Error en renderizarVideojuegosController", error);
    res.status(500).json({
      message: `Error en la obtención de los registros desde IGDB`,
    });
  }
};

export { renderizarVideojuegosController };
