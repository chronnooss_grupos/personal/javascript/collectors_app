import { guardarJSONHelper } from "./helper.guardarArchivo.js";
import {
  formatearFechaHelper,
  convertirCadenaANumeros,
} from "./helper.util.js";
import * as dotenv from "dotenv";
dotenv.config();

const construirObjBusquedaVideojuegosHelper = async (
  ve_search,
  ve_platform,
  ve_categories
) => {
  let p_search = ve_search ?? "";
  let p_where = "";

  if (ve_platform.length > 0) {
    p_where = `platforms = (${ve_platform.join(",")})`;
  } else {
    p_where = `platforms = (${process.env.IGDB_PLATFORMS_DEFAULT})`;
  }

  if (ve_categories.length > 0) {
    p_where += ` & category = (${ve_categories.join(",")})`;
  } else {
    p_where += ` & category = (${process.env.IGDB_CATEGORIES_DEFAULT})`;
  }

  const objBusqueda = {
    fields: process.env.IGDB_FIELDS_DB,
    search: p_search,
    where: p_where,
    limit: process.env.IGDB_LIMIT,
    sort: process.env.IGDB_SORT,
    exclude: process.env.IGDB_EXCLUDE,
    offset: process.env.IGDB_OFFSET,
  };

  return objBusqueda;
};

const construirQueryIGDBHelper = (objBusqueda) => {
  let vl_query = "";
  let { fields, search, where, limit, sort, exclude, offset } = objBusqueda;

  if (fields != "") {
    vl_query += `fields ${fields};`;
  }

  if (search != "") {
    vl_query += `search "${search}";`;
  }

  if (where != "") {
    vl_query += `where ${where};`;
  }

  if (limit != "") {
    vl_query += `limit ${limit};`;
  }

  if (sort != "") {
    vl_query += `sort ${sort};`;
  }

  if (exclude != "") {
    vl_query += `exclude ${exclude};`;
  }

  if (offset != "") {
    vl_query += `offset ${offset};`;
  }

  return vl_query;
};

const construirQueryOffsetIGDBHelper = (objBusqueda, ve_offset) => {
  let vl_query = "";
  let { fields, search, where, limit, sort, exclude, offset } = objBusqueda;

  if (fields != "") {
    vl_query += `fields ${fields};`;
  }

  if (search != "") {
    vl_query += `search "${search}";`;
  }

  if (where != "") {
    vl_query += `where ${where};`;
  }

  if (limit != "") {
    vl_query += `limit ${limit};`;
  }

  if (sort != "") {
    vl_query += `sort ${sort};`;
  }

  if (exclude != "") {
    vl_query += `exclude ${exclude};`;
  }

  if (ve_offset != "") {
    vl_query += `offset ${ve_offset};`;
  }

  return vl_query;
};

const obtenerArrJuegosIGDBHelper = async (respJuegos, objVideojuegos) => {
  try {
    for (const juego of respJuegos) {
      let arrPlataformas = construirArrPlataformasIGDBHelper(juego.platforms);
      let arrGeneros = construirArrGenerosIGDBHelper(juego.genres);
      let arrDesarrollador = construirArrDesarrolladorIGDBHelper(
        juego.involved_companies
      );
      let arrPublicador = construirArrPublicadorIGDBHelper(
        juego.involved_companies
      );

      const idJuego = juego.id;
      const nomJuego = juego.name.replace(/"/g, "");
      const idImageJuego = juego.cover ? juego.cover.image_id : "";
      const urlImagenJuego = juego.cover
        ? `https://images.igdb.com/igdb/image/upload/t_720p/${idImageJuego}.jpg`
        : `${process.env.IGDB_IMAGE_EMPTY}`;
      const fechaSalida = juego.first_release_date
        ? formatearFechaHelper(juego.first_release_date)
        : "S/I";

      const objRespuesta = {
        id: idJuego,
        nombre: nomJuego,
        imagen: urlImagenJuego,
        plataformas: arrPlataformas,
        generos: arrGeneros,
        desarrollador: arrDesarrollador,
        publicador: arrPublicador,
        fecha: fechaSalida,
      };

      objVideojuegos.push(objRespuesta);
    }

    // Guardar datos en archivo JSON
    objVideojuegos.sort((a, b) => {
      const nombreA = a.nombre.toLowerCase();
      const nombreB = b.nombre.toLowerCase();

      if (nombreA < nombreB) {
        return -1;
      }
      if (nombreA > nombreB) {
        return 1;
      }
      return 0;
    });

    guardarJSONHelper(objVideojuegos);

    return objVideojuegos;
  } catch (error) {
    console.log("Error en obtenerArrJuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

const renderizarPanelGaleriaVideojuegosIGDBHelper = async (arrVideojuegos) => {
  try {
    const buildBadges = (items, className) =>
      items
        .map(
          (item) =>
            `<span class='badge rounded-pill badge-${
              item.color
            }'>${item.abbrev.toUpperCase()}</span>`
        )
        .join(" ");

    const buildOptions = (items) =>
      items
        .map(
          (item, index) =>
            `<option value="${index + 1}">${item.abbrev.toUpperCase()}</option>`
        )
        .join(" ");

    const buildGaleriaItems = (videojuegos) =>
      videojuegos
        .map(
          (element) => `
      <div class="galeria-item">
        <img class="galeria-picture" src="${element.imagen}" alt="${
            element.nombre
          }">
        <div class="content">
          <div class="short-desc">
            ${buildBadges(element.plataformas)}
          </div>
          <label hidden="hidden">${element.id}</label>
          <h5>${element.fecha}</h5>
          <h4>${element.nombre}</h4>
          <div class="footer">
            <a href="#"><i class="fa-solid fa-heart"></i></a>
            <a href="#" onclick="guardarElemento(${
              element.id
            }, event, this);"><i class="fa-solid fa-bookmark" aria-hidden="true"></i></a>
            <a href="#"><i class="fa-solid fa-share"></i></a>
          </div>
        </div>
      </div>`
        )
        .join(" ");

    let htmlVideojuegos = `<div class="cardLibrary card container m-auto p-3"><div class="galeria" id="galeria">`;

    htmlVideojuegos += buildGaleriaItems(arrVideojuegos);

    htmlVideojuegos += `</div></div>`;

    return htmlVideojuegos;
  } catch (error) {
    console.log("Error en renderizarPanelGaleriaVideojuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

const renderizarPanelTablaVideojuegosIGDBHelper = async (arrVideojuegos) => {
  try {
    const buildBadges = (items, className) =>
      items
        .map(
          (item) =>
            `<span class='badge rounded-pill badge-${
              item.color
            }'>${item.abbrev.toUpperCase()}</span>`
        )
        .join(" ");

    const buildOptions = (items) =>
      items
        .map(
          (item, index) =>
            `<option value="${index + 1}">${item.abbrev.toUpperCase()}</option>`
        )
        .join(" ");

    const buildTablaItems = (videojuegos) =>
      videojuegos
        .map(
          (element) => `
          <tr>
          <td hidden="hidden">${element.id}</td>
          <td><img src="${element.imagen}" style="width: 70px;
          height: auto;
          /* width: 220px;
            height: 300px; */
          transition: 0.2s linear;
          transform-origin: 50% 50%;
          border-radius: 15px;" alt="Imagen"></td>
          
          <td>${element.nombre}</td>
          <td>${buildBadges(element.plataformas)}</td>
          <td>${element.fecha}</td>
        </tr>`
        )
        .join(" ");

    let htmlVideojuegos = `<table class='table'>
                            <thead>
                            <tr>
                              <th hidden="hidden">ID</th>
                              <th>Imagen</th>
                              <th>Título</th>
                              <th>Plataformas</th>
                              <th>Fecha</th>
                            </tr>
                          </thead>
                          <tbody>`;

    htmlVideojuegos += buildTablaItems(arrVideojuegos);

    htmlVideojuegos += `</tbody></table>`;

    return htmlVideojuegos;
  } catch (error) {
    console.log("Error en renderizarPanelTablaVideojuegosIGDBHelper", error);
    throw new Error(`Error: ${error.body}`);
  }
};

const construirJuego = (juego) => {
  // Declaración de variables
  let juegoId = "";
  let juegoName = "";
  let juegoFirstReleaseDate = "";
  let juegoPlatforms = [];
  let juegoThemes = [];
  let juegoGenres = [];
  let juegoCoverImageId = "";
  let juegoCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
  let juegoScreenshots = [];
  let juegoVideos = [];
  let juegoDevelopers = [];
  let juegoPublishers = [];
  let juegoLanguages = [];
  let juegoBundles = [];
  let juegoCollections = [];
  let juegoDlcs = [];
  let juegoExpandedGames = [];
  let juegoExpansions = [];
  // let juegoFranchises = [];
  let juegoGameLocalizations = [];
  let juegoParentGame = {};
  let juegoPorts = [];
  let juegoRemakes = [];

  // Seteo de variables
  // ···Id
  if (juego.id) {
    juegoId = juego.id;
  }

  // ···Name
  if (juego.name) {
    juegoName = juego.name.replace(/"/g, "");
  }

  // ···First Release Date
  if (juego.first_release_date) {
    juegoFirstReleaseDate = formatearFechaHelper(juego.first_release_date);
  }

  // ···Platforms
  if (juego.platforms) {
    if (juego.platforms.length > 0) {
      let arrNintendo = convertirCadenaANumeros(
        process.env.IGDB_PLATFORMS_NINTENDO_SOLO
      );
      let arrSony = convertirCadenaANumeros(
        process.env.IGDB_PLATFORMS_SONY_SOLO
      );
      let arrMicrosoft = convertirCadenaANumeros(
        process.env.IGDB_PLATFORMS_MICROSOFT_SOLO
      );
      let arrPC = convertirCadenaANumeros(process.env.IGDB_PLATFORMS_PC_SOLO);
      let arrMobile = convertirCadenaANumeros(
        process.env.IGDB_PLATFORMS_MOBILE_SOLO
      );

      let itemPlatform;

      for (const item of juego.platforms) {
        if (item.abbreviation) {
          const obtAbbreviation = item.abbreviation
            .replace(/^NES/, "NES")
            .replace(/^SNES/, "SNES")
            .replace(/^N64/, "N64")
            .replace(/^NGC/, "GCN")
            .replace(/^Wii/, "WII")
            .replace(/^WiiU/, "WIIU")
            .replace(/^Switch/, "SWITCH")
            .replace(/^Game Boy/, "GB")
            .replace(/^GBC/, "GBC")
            .replace(/^GBA/, "GBA")
            .replace(/^NDS/, "NDS")
            .replace(/^3DS/, "3DS")
            .replace(/^PS1/, "PS1")
            .replace(/^PS2/, "PS2")
            .replace(/^PS3/, "PS3")
            .replace(/^PS4/, "PS4")
            .replace(/^PS5/, "PS5")
            .replace(/^PSP/, "PSP")
            .replace(/^Vita/, "PSVITA")
            .replace(/^PlayStation VR/, "PSVR")
            .replace(/^PSVR2/, "PSVR2")
            .replace(/^XBOX/, "XBOX")
            .replace(/^X360/, "X360")
            .replace(/^XONE/, "XONE")
            .replace(/^Series X/, "XSERIES")
            .replace(/^PC/, "PC")
            .replace(/^iOS/, "IOS")
            .replace(/^Android/, "ANDROID");

          if (arrNintendo.includes(item.id)) {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `red`,
            };
          } else if (arrSony.includes(item.id)) {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `blue`,
            };
          } else if (arrMicrosoft.includes(item.id)) {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `green`,
            };
          } else if (arrPC.includes(item.id)) {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `brown`,
            };
          } else if (arrMobile.includes(item.id)) {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `yellow`,
            };
          } else {
            itemPlatform = {
              abbrev: `${obtAbbreviation}`,
              name: `${item.name}`,
              color: `default`,
            };
          }
          juegoPlatforms.push(itemPlatform);
        }
      }
    }
  }

  // ···Themes
  if (juego.themes) {
    if (juego.themes.length > 0) {
      let itemTheme;
      for (const item of juego.themes) {
        itemTheme = [
          {
            name: `${item.name}`,
            color: "default",
          },
        ];
        juegoThemes.push(itemTheme);
      }
    }
  }

  // ···Genres
  if (juego.genres) {
    if (juego.genres.length > 0) {
      let itemGenre;
      for (const item of juego.genres) {
        itemGenre = [
          {
            name: `${item.name}`,
            color: "default",
          },
        ];
        juegoGenres.push(itemGenre);
      }
    }
  }

  // ···Involved_companies.developer
  if (juego.involved_companies) {
    if (juego.involved_companies.length > 0) {
      for (const item of juego.involved_companies) {
        let itemDeveloper;
        if (item.developer) {
          itemDeveloper = {
            name: `${item.company.name}`,
            color: "default",
          };
          juegoDevelopers.push(itemDeveloper);
        }
      }
    }
  }

  // ···Involved_companies.publisher
  if (juego.involved_companies) {
    if (juego.involved_companies.length > 0) {
      for (const item of juego.involved_companies) {
        let itemPublisher;
        if (item.publisher) {
          itemPublisher = {
            name: `${item.company.name}`,
            color: "default",
          };
          juegoPublishers.push(itemPublisher);
        }
      }
    }
  }

  // ···Cover.image_id
  if (juego.cover) {
    juegoCoverImageId = juego.cover.image_id;
    juegoCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${juegoCoverImageId}.jpg`;
  }

  // ···Screenshots.image_id
  if (juego.screenshots) {
    if (juego.screenshots.length > 0) {
      let juegoScreenshotImageId = "";
      let juegoScreenshotImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
      for (const item of juego.screenshots) {
        juegoScreenshotImageId = item.image_id;
        juegoScreenshotImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${juegoScreenshotImageId}.jpg`;
        juegoScreenshots.push(juegoScreenshotImageUrl);
      }
    }
  }

  // ···Videos.video_id
  if (juego.videos) {
    if (juego.videos.length > 0) {
      let juevoVideoId = "";
      let juevoVideoUrl = "";
      for (const item of juego.videos) {
        juevoVideoId = juego.videos.video_id;
        juevoVideoUrl = `https://www.youtube.com/watch?v=${juevoVideoId}`;
        juegoVideos.push(juevoVideoUrl);
      }
    }
  }

  // ···language_supports.language
  if (juego.language_supports) {
    if (juego.language_supports.length > 0) {
      for (const itemA of juego.language_supports) {
        let itemLanguage;
        if (itemA.language) {
          itemLanguage = {
            name: `${itemA.language.name}`,
            nativeName: `${itemA.language.native_name}`,
            color: "default",
          };
          juegoLanguages.push(itemLanguage);
        }
      }
    }
  }

  // ···Bundles
  if (juego.bundles) {
    if (juego.bundles.length > 0) {
      let itemBundle;
      for (const item of juego.bundles) {
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemBundle = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoBundles.push(itemBundle);
      }
    }
  }

  // ···Collections
  if (juego.collection) {
    if (juego.collection.games) {
      if (juego.collection.games.length > 0) {
        let itemCollection;
        for (const item of juego.collection.games) {
          let itemCoverImageId = "";
          let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
          if (item.cover) {
            itemCoverImageId = item.cover.image_id;
            itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
          }
          itemCollection = [
            {
              gameName: `${item.name}`,
              gameFirstReleaseDate: `${item.first_release_date}`,
              gameCoverImageId: `${itemCoverImageUrl}`,
              color: "default",
            },
          ];
          juegoCollections.push(itemCollection);
        }
      }
    }
  }

  // ···Dlcs
  if (juego.dlcs) {
    if (juego.dlcs.length > 0) {
      let itemDlcs;
      for (const item of juego.dlcs) {
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemDlcs = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoDlcs.push(itemDlcs);
      }
    }
  }

  // ···Expanded Games
  if (juego.expanded_games) {
    if (juego.expanded_games.length > 0) {
      let itemExpandedGames;
      for (const item of juego.expanded_games) {
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemExpandedGames = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoExpandedGames.push(itemExpandedGames);
      }
    }
  }

  // ···Expansions
  if (juego.expansions) {
    if (juego.expansions.length > 0) {
      let itemExpansions;
      for (const item of juego.expansions) {
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemExpansions = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoExpansions.push(itemExpansions);
      }
    }
  }

  // ···Game_localizations.region
  if (juego.game_localizations) {
    if (juego.game_localizations.length > 0) {
      let itemGameLocalizationRegion;
      for (const item of juego.game_localizations) {
        itemGameLocalizationRegion = [
          {
            nameRegion: `${item.name}`,
            color: "default",
          },
        ];
        juegoGameLocalizations.push(itemGameLocalizationRegion);
      }
    }
  }

  // ···Parent_game
  if (juego.parent_game) {
    let itemCoverImageId = "";
    let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
    if (juego.parent_game.cover) {
      itemCoverImageId = juego.parent_game.cover.image_id;
      itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
    }
    juegoParentGame = {
      gameName: `${juego.parent_game.name}`,
      gameFirstReleaseDate: `${juego.parent_game.first_release_date}`,
      gameCoverImageId: `${itemCoverImageUrl}`,
      color: "default",
    };
  }

  // ···Ports
  if (juego.ports) {
    if (juego.ports.length > 0) {
      let itemPort;
      for (const item of juego.ports) {
        console.log("FLAG");
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemPort = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoPorts.push(itemPort);
      }
    }
  }

  // ···Remakes
  if (juego.remakes) {
    if (juego.remakes.length > 0) {
      let itemRemakes;
      for (const item of juego.remakes) {
        let itemCoverImageId = "";
        let itemCoverImageUrl = `${process.env.IGDB_IMAGE_EMPTY}`;
        if (item.cover) {
          itemCoverImageId = item.cover.image_id;
          itemCoverImageUrl = `https://images.igdb.com/igdb/image/upload/t_720p/${itemCoverImageId}.jpg`;
        }
        itemRemakes = [
          {
            gameName: `${item.name}`,
            gameFirstReleaseDate: `${item.first_release_date}`,
            gameCoverImageId: `${itemCoverImageUrl}`,
            color: "default",
          },
        ];
        juegoRemakes.push(itemRemakes);
      }
    }
  }

  const objRespuesta = {
    id: juegoId,
    nombre: juegoName,
    imagen: juegoCoverImageUrl,
    plataformas: juegoPlatforms,
    generos: juegoGenres,
    desarrolladores: juegoDevelopers,
    publicadores: juegoPublishers,
    fecha: juegoFirstReleaseDate,
  };

  const objRespuestaAux = {
    game: {
      id: juegoId,
      nombre: juegoName,
      imagen: juegoCoverImageUrl,
      fecha: juegoFirstReleaseDate,
    },
    plataformas: juegoPlatforms,
    generos: juegoGenres,
    desarrolladores: juegoDevelopers,
    publicadores: juegoPublishers,
    themes: juegoThemes,
    screenshots: juegoScreenshots,
    videos: juegoVideos,
    languages: juegoLanguages,
    bundles: juegoBundles,
    collections: juegoCollections,
    dlcs: juegoDlcs,
    expandedGames: juegoExpandedGames,
    expansiones: juegoExpansions,
    gameExpansions: juegoExpansions,
    gameLocalizations: juegoGameLocalizations,
    parentGame: juegoParentGame,
    ports: juegoPorts,
    remakes: juegoRemakes,
  };

  // console.log(objRespuestaAux);

  if (juego.remakes) {
    console.log(objRespuestaAux);
  }

  return objRespuesta;
};

export {
  construirObjBusquedaVideojuegosHelper,
  construirQueryIGDBHelper,
  construirQueryOffsetIGDBHelper,
  obtenerArrJuegosIGDBHelper,
  renderizarPanelGaleriaVideojuegosIGDBHelper,
  renderizarPanelTablaVideojuegosIGDBHelper,
  construirJuego,
};
