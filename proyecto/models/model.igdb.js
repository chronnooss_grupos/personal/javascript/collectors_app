import * as dotenv from "dotenv";
import axios from "axios";
dotenv.config();
import {
  construirQueryIGDBHelper,
  construirQueryOffsetIGDBHelper,
  obtenerArrJuegosIGDBHelper,
  construirJuego,
} from "../helpers/helper.igdb.js";
import { formatearFechaHelper } from "../helpers/helper.util.js";

const autenticarConIGDBModel = async () => {
  const response = await axios.post(`${process.env.IGDB_AUTH_URL}`);
  return response.data;
};

const cantidadJuegosIGDBModel = async (objBusqueda) => {
  // Obtener Access Token de IGDB
  let { access_token } = await autenticarConIGDBModel();

  const url = process.env.IGDB_ENDPOINT_CANTIDAD;
  const headers = {
    headers: {
      Accept: "application/json",
      "Client-ID": process.env.IGDB_CLIENT_ID,
      Authorization: `Bearer ${access_token}`,
    },
  };

  // Construir Query para IGDB
  const query = construirQueryIGDBHelper(objBusqueda);

  // Llamada a API IGDB para obtener cantidad de juegos
  const response = await axios.post(url, query, headers);

  return response;
};

const obtenerJuegosIGDBModel = async (objBusqueda, cantidadVideojuegos) => {
  try {
    const arrVideojuegos = [];
    let offset = 0;

    do {
      // Obtener Access Token de IGDB
      const { access_token } = await autenticarConIGDBModel();
      const headers = {
        headers: {
          Accept: "application/json",
          "Client-ID": process.env.IGDB_CLIENT_ID,
          Authorization: `Bearer ${access_token}`,
        },
      };
      console.log("access_token", access_token);
      const url = process.env.IGDB_ENDPOINT_JUEGOS;
      const query = construirQueryOffsetIGDBHelper(objBusqueda, offset);

      const response = await axios.post(url, query, headers);

      let respJuegos = response.data;

      for (const juego of respJuegos) {
        let objRespuesta = construirJuego(juego);
        arrVideojuegos.push(objRespuesta);
      }

      offset += 500;
    } while (arrVideojuegos.length != cantidadVideojuegos);

    // Guardar datos en archivo JSON
    arrVideojuegos.sort((a, b) => {
      const nombreA = a.nombre.toLowerCase();
      const nombreB = b.nombre.toLowerCase();

      if (nombreA < nombreB) {
        return -1;
      }
      if (nombreA > nombreB) {
        return 1;
      }
      return 0;
    });

    return arrVideojuegos.slice(0, cantidadVideojuegos);
  } catch (error) {
    console.log("Error en obtenerJuegosIGDBModel", error);
    throw new Error(`${error.body}`);
  }
};

export { cantidadJuegosIGDBModel, obtenerJuegosIGDBModel };
