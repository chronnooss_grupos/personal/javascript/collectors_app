// Variables html
let form = document.getElementById("formBusquedaJuegos");
let formGamesLibrary = document.getElementById("formGamesLibrary");
let cantidadVideojuegos = document.getElementById("cantidadVideojuegos");
let panelLibreria = document.getElementById("panelLibreria");
let panelTabla = document.getElementById("panelTabla");
let btnBuscarJuego = document.querySelector("#btnBuscarJuego");
// Variables de servicio
let urlServicioRenderizarVideojuegos =
  "http://localhost:8080/api/igdb/renderizarVideojuegos";
let postDataRenderizarVideojuegos = "";
let requestOptionsRenderizarVideojuegos = "";

form.addEventListener("submit", async (e) => {
  // Prevenir el envío del formulario por defecto
  e.preventDefault();

  try {
    // Deshabilitar el botón de búsqueda y mostrar el spinner de carga
    btnBuscarJuego.disabled = true;
    btnBuscarJuego.querySelector(".spinner-border").classList.remove("d-none");

    // Cargar panel de videojuegos
    await cargaPanelVideojuegos();

    // Habilitar el botón de búsqueda y ocultar el spinner de carga
    btnBuscarJuego.disabled = false;
    btnBuscarJuego.querySelector(".spinner-border").classList.add("d-none");
    formGamesLibrary.classList.remove("d-none");
  } catch (error) {
    // Manejar errores
    console.log(
      "Error en el submit del formulario de búsqueda de juegos.",
      error
    );
  }
});

const cargaPanelVideojuegos = async () => {
  const inputSearch = $("#inputSearch").val();
  // const selectPlatform = $("#selectPlatform option:selected").val();
  const selectPlatform = $("#selectPlatform").val();
  const selectCategories = $("#selectCategories").val();

  try {
    let search = inputSearch || "";

    let platforms = selectPlatform;
    // selectPlatform.length > 0 && selectPlatform != 0 ? selectPlatform : "";
    let categoriesQuery = selectCategories;
    // categories.length > 0 ? `category = (${categories.join(",")})` : "";

    const postDataRenderizarVideojuegos = {
      search,
      platforms,
      categories: categoriesQuery,
    };

    const requestOptionsRenderizarVideojuegos = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postDataRenderizarVideojuegos),
    };

    const respuestaRenderizarVideojuegos = await fetch(
      urlServicioRenderizarVideojuegos,
      requestOptionsRenderizarVideojuegos
    );
    const respuestaJsonRenderizarVideojuegos =
      await respuestaRenderizarVideojuegos.json();

    const cantidadVideojuegosElement = document.querySelector(
      "#cantidadVideojuegos"
    );
    const galeriaElement = document.querySelector("#galeria");
    const tablaElement = document.querySelector("#tablaVideojuegos");

    cantidadVideojuegosElement.innerHTML =
      respuestaJsonRenderizarVideojuegos.htmlTotalJuegos;
    galeriaElement.innerHTML =
      respuestaJsonRenderizarVideojuegos.htmlPanelGaleriaVideojuegos;
    tablaElement.innerHTML =
      respuestaJsonRenderizarVideojuegos.htmlPanelTablaVideojuegos;
  } catch (error) {
    console.log("Error en cargaPanelVideojuegos", error);
  }
};
